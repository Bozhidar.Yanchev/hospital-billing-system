In this exercise, you will prepare а class diagram for a hospital billing 
system.

a. Add relevant class(es) to store information about doctors, and the 
doctor’s specialties.

b. Add relevant class(es) to store information about bills, esp. the 
patient’s ID and the patient’s hospital charges such as pharmacy charges 
for medicine, the doctor’s fee, and the room charges. 

c. Add relevant class(es) to store information about patients, esp. a 
patient’s ID, age, date of birth, and attending physician’s name. 

d. Add relevant class(es) to store information about hospital admissions 
for the patients - e.g. the date when the patient was admitted in the 
hospital, the date when the patient was discharged from the hospital, 
diagnosis and course of treatment.

Make sure to show appropriate constructors and (at least) the methods to 
initialize, access, and manipulate the data members.

# **Hospital Billing System**

Hospital Billing System is a system that stores any kind of information needed since the admission of a patient
in the hospital until his discharge. When a discharge date is inserted the system calculates the final bill that must
be paid by the patient.

The app supports:

* Swagger documentation: http://localhost:8080/swagger-ui/
* JDK 11

**Data Base structure and connections:**

[![hospital_billing_system_database.png](https://i.postimg.cc/CLD2qHmm/hospital-billing-system-database.png)](https://postimg.cc/1fmMx6xq)


**SWAGGER PHOTOS**

[![swagger1.png](https://i.postimg.cc/Pr62tm96/sw-agger1.png)](https://postimg.cc/KRMnfgZL)


[![swagger2.png](https://i.postimg.cc/0jVdysW6/swagger2.png)](https://postimg.cc/q6CC19d0)


[![swagger3.png](https://i.postimg.cc/T15YpjQr/swagger3.png)](https://postimg.cc/ctZNznpH)


[![swagger4.png](https://i.postimg.cc/sxWQtRFd/swagger4.png)](https://postimg.cc/hQStd6Pp)


[![swagger5.png](https://i.postimg.cc/3NhYxM4H/swagger5.png)](https://postimg.cc/mzm0m5yd)


[![swagger6.png](https://i.postimg.cc/9Qk2qwRb/swagger6.png)](https://postimg.cc/pmK4wTB5)


[![swagger7.png](https://i.postimg.cc/8kbVm0fj/swagger7.png)](https://postimg.cc/7bbcxXVk)


