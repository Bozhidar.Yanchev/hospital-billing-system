package com.adastra.billing_system.models.dtos;

import java.util.Date;

public class AdmissionDischargeDto {

    private Date dischargeDate;

    public AdmissionDischargeDto() {
    }
    public Date getDischargeDate() {
        return dischargeDate;
    }

    public void setDischargeDate(Date dischargeDate) {
        this.dischargeDate = dischargeDate;
    }
}
