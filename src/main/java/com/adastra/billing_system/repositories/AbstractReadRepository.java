package com.adastra.billing_system.repositories;

import com.adastra.billing_system.exceptions.EntityNotFoundException;
import com.adastra.billing_system.repositories.contracts.BaseReadRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class AbstractReadRepository<T> implements BaseReadRepository<T> {

    private final Class<T> tClass;
    protected final SessionFactory sessionFactory;

    public AbstractReadRepository(Class<T> tClass, SessionFactory sessionFactory) {
        this.tClass = tClass;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<T> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(String.format("from %s ", tClass.getName()), tClass).list();
        }
    }

    @Override
    public T getById(int id) {
        return getByField("id", id);
    }

    @Override
    public <V> T getByField(String name, V value) {
        final String query = String.format("from %s where %s = :value", tClass.getName(), name);

        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery(query, tClass)
                    .setParameter("value", value)
                    .uniqueResultOptional()
                    .orElseThrow(() -> new EntityNotFoundException(tClass.getSimpleName(), name, String.valueOf(value)));
        }
    }
}
