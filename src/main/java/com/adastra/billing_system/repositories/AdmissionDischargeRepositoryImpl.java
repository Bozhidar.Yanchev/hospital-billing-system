package com.adastra.billing_system.repositories;

import com.adastra.billing_system.exceptions.EntityNotFoundException;
import com.adastra.billing_system.models.Admission;
import com.adastra.billing_system.models.AdmissionDischarge;
import com.adastra.billing_system.repositories.contracts.AdmissionDischargeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AdmissionDischargeRepositoryImpl extends AbstractCrudRepository<AdmissionDischarge>
        implements AdmissionDischargeRepository {

    @Autowired
    public AdmissionDischargeRepositoryImpl(SessionFactory sessionFactory) {
        super(AdmissionDischarge.class, sessionFactory);
    }

    @Override
    public AdmissionDischarge getByAdmission(Admission admission) {
        try (Session session = sessionFactory.openSession()) {
            Query<AdmissionDischarge> query = session.createQuery(
                    "from AdmissionDischarge where admission = :value",
                    AdmissionDischarge.class);
            query.setParameter("value", admission);
            List<AdmissionDischarge> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Admission", admission.getAdmissionId());
            }
            return result.get(0);
        }
    }
}
