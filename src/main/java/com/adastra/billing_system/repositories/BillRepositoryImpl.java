package com.adastra.billing_system.repositories;

import com.adastra.billing_system.models.Bill;
import com.adastra.billing_system.models.Patient;
import com.adastra.billing_system.repositories.contracts.BillRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class BillRepositoryImpl extends AbstractCrudRepository<Bill> implements BillRepository {

    @Autowired
    public BillRepositoryImpl(SessionFactory sessionFactory) {
        super(Bill.class, sessionFactory);
    }

    @Override
    public List<Bill> getByPatient(Patient patient, Date admissionDate, Date dischargeDate) {
        try (Session session = sessionFactory.openSession()) {
            Query<Bill> query = session.createQuery(
                    "from Bill b " +
                            "where b.patient = :value and b.date >= :admissionDate and b.date <= :dischargeDate",
                    Bill.class);
            query.setParameter("value", patient);
            query.setParameter("admissionDate", admissionDate);
            query.setParameter("dischargeDate", dischargeDate);

            return query.list();
        }
    }
}
