package com.adastra.billing_system.repositories;

import com.adastra.billing_system.models.Doctor;
import com.adastra.billing_system.repositories.contracts.DoctorRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DoctorRepositoryImpl extends AbstractCrudRepository<Doctor> implements DoctorRepository {

    @Autowired
    public DoctorRepositoryImpl(SessionFactory sessionFactory) {
        super(Doctor.class, sessionFactory);
    }
}
