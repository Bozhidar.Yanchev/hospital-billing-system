package com.adastra.billing_system.repositories;

import com.adastra.billing_system.models.Admission;
import com.adastra.billing_system.models.Patient;
import com.adastra.billing_system.repositories.contracts.AdmissionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AdmissionRepositoryImpl extends AbstractCrudRepository<Admission> implements AdmissionRepository {

    @Autowired
    public AdmissionRepositoryImpl(SessionFactory sessionFactory) {
        super(Admission.class, sessionFactory);
    }

    @Override
    public List<Admission> getByPatient(Patient patient) {
        try (Session session = sessionFactory.openSession()) {
            Query<Admission> query = session.createQuery("from Admission where patient = :value",
                    Admission.class);
            query.setParameter("value", patient);

            return query.list();
        }
    }


}
