package com.adastra.billing_system.repositories.contracts;

import com.adastra.billing_system.models.Doctor;

public interface DoctorRepository extends BaseCrudRepository<Doctor> {
}
