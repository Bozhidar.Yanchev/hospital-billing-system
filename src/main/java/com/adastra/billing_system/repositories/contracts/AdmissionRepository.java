package com.adastra.billing_system.repositories.contracts;

import com.adastra.billing_system.models.Admission;
import com.adastra.billing_system.models.Patient;

import java.util.List;

public interface AdmissionRepository extends BaseCrudRepository<Admission> {

    List<Admission> getByPatient(Patient patient);
}
