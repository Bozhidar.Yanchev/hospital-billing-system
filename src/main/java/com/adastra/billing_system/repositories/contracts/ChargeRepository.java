package com.adastra.billing_system.repositories.contracts;

import com.adastra.billing_system.models.Charge;
public interface ChargeRepository extends BaseCrudRepository<Charge> {
}
