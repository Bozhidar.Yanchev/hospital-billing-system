package com.adastra.billing_system.repositories.contracts;

import com.adastra.billing_system.models.Bill;
import com.adastra.billing_system.models.Patient;

import java.util.Date;
import java.util.List;

public interface BillRepository extends BaseCrudRepository<Bill> {
    List<Bill> getByPatient(Patient patient, Date admissionDate, Date dischargeDate);
}
