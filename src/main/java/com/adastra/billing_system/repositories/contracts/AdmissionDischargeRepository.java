package com.adastra.billing_system.repositories.contracts;

import com.adastra.billing_system.models.Admission;
import com.adastra.billing_system.models.AdmissionDischarge;

public interface AdmissionDischargeRepository extends BaseCrudRepository<AdmissionDischarge> {

    AdmissionDischarge getByAdmission(Admission admission);
}
