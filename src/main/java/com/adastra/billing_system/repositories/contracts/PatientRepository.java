package com.adastra.billing_system.repositories.contracts;

import com.adastra.billing_system.models.Patient;

public interface PatientRepository extends BaseCrudRepository<Patient> {
}
