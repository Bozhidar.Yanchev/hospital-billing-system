package com.adastra.billing_system.repositories;

import com.adastra.billing_system.models.Patient;
import com.adastra.billing_system.repositories.contracts.PatientRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PatientRepositoryImpl extends AbstractCrudRepository<Patient> implements PatientRepository {

    @Autowired
    public PatientRepositoryImpl(SessionFactory sessionFactory) {
        super(Patient.class, sessionFactory);
    }
}
