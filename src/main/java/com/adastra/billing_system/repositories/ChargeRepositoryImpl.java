package com.adastra.billing_system.repositories;

import com.adastra.billing_system.models.Charge;
import com.adastra.billing_system.repositories.contracts.ChargeRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ChargeRepositoryImpl extends AbstractCrudRepository<Charge> implements ChargeRepository {

    @Autowired
    public ChargeRepositoryImpl(SessionFactory sessionFactory) {
        super(Charge.class, sessionFactory);
    }

}
