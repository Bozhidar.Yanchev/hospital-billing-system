package com.adastra.billing_system.controllers;

import com.adastra.billing_system.exceptions.EntityNotFoundException;
import com.adastra.billing_system.exceptions.NoDischargeDateException;
import com.adastra.billing_system.models.Admission;
import com.adastra.billing_system.models.AdmissionDischarge;
import com.adastra.billing_system.models.Patient;
import com.adastra.billing_system.services.contracts.AdmissionService;
import com.adastra.billing_system.services.contracts.PatientService;
import com.adastra.billing_system.services.helpers.DateFormatterHelper;
import com.adastra.billing_system.services.mappers.AdmissionDischargeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/api/admissions")
public class AdmissionControllerImpl {

    private final AdmissionService admissionService;

    private final PatientService patientService;

    private final AdmissionDischargeMapper admissionDischargeMapper;

    private final DateFormatterHelper dateFormatterHelper;

    @Autowired
    public AdmissionControllerImpl(AdmissionService admissionService,
                                   PatientService patientService,
                                   AdmissionDischargeMapper admissionDischargeMapper,
                                   DateFormatterHelper dateFormatterHelper) {
        this.admissionService = admissionService;
        this.patientService = patientService;
        this.admissionDischargeMapper = admissionDischargeMapper;
        this.dateFormatterHelper = dateFormatterHelper;
    }

    @GetMapping("/all")
    public List<Admission> getAllAdmissions() {
        return admissionService.getAll();
    }

    @GetMapping("/{id}")
    public Admission getById(@PathVariable int id) {
        try {
            return admissionService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/patient/{patientId}")
    public List<Admission> getByPatient(@PathVariable int patientId) {
        try {
            Patient patient = patientService.getById(patientId);
            return admissionService.getByPatient(patient);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/dischargeDate")
    public AdmissionDischarge checkAdmissionDischargeDate(@PathVariable int id) {
        try {
            Admission admission = admissionService.getById(id);
            return admissionService.checkAdmissionDischargeDate(admission);
        } catch (EntityNotFoundException | NoDischargeDateException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @PostMapping
    public Admission createAdmission(@RequestBody Admission admission) {
        admissionService.createAdmission(admission);

        return admission;
    }

    @PutMapping("/{id}")
    public Admission updateAdmission(@PathVariable int id, @RequestBody Admission admission) {
        try {
            admissionService.getById(id);
            admissionService.updateAdmission(admission);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return admission;
    }

    @DeleteMapping("/{id}")
    public void deleteAdmission(@PathVariable int id) {
        try {
            admissionService.deleteAdmission(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{id}/dischargeDate")
    public AdmissionDischarge createAdmissionDischarge(@PathVariable int id,
                                                       @RequestBody String dischargeDate) throws ParseException {
        try {
            AdmissionDischarge admissionDischarge = new AdmissionDischarge();
            admissionDischarge.setDischargeDate(dateFormatterHelper.dateFormatter(dischargeDate));

            Admission admission = admissionService.getById(id);
            admissionDischarge.setAdmission(admission);
            admissionService.createAdmissionDischarge(admissionDischarge);
            return admissionDischarge;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
