package com.adastra.billing_system.controllers;

import com.adastra.billing_system.exceptions.EntityNotFoundException;
import com.adastra.billing_system.models.Bill;
import com.adastra.billing_system.models.Patient;
import com.adastra.billing_system.services.contracts.BillService;
import com.adastra.billing_system.services.contracts.PatientService;
import com.adastra.billing_system.services.helpers.DateFormatterHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/bills")
public class BillControllerImpl {

    private final BillService billService;

    private final PatientService patientService;

    private final DateFormatterHelper dateFormatterHelper;

    @Autowired
    public BillControllerImpl(BillService billService,
                              PatientService patientService,
                              DateFormatterHelper dateFormatterHelper) {
        this.billService = billService;
        this.patientService = patientService;
        this.dateFormatterHelper = dateFormatterHelper;
    }

    @GetMapping("/all")
    public List<Bill> getAllBills() {
        return billService.getAll();
    }

    @GetMapping("/{id}")
    public Bill getById(@PathVariable int id) {
        try {
            return billService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/patient/{patientId}")
    public List<Bill> getByPatient(@PathVariable int patientId,
                                   @RequestParam String admissionDate,
                                   @RequestParam String dischargeDate) throws ParseException {
        try {

            Date dateAdmit = dateFormatterHelper.dateFormatter(admissionDate);
            Date dateDischarge = dateFormatterHelper.dateFormatter(dischargeDate);

            Patient patient = patientService.getById(patientId);
            return billService.getByPatient(patient, dateAdmit, dateDischarge);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Bill createBill(@RequestBody Bill bill) {
        billService.createBill(bill);

        return bill;
    }

    @PutMapping("/{id}")
    public Bill updateBill(@PathVariable int id, @RequestBody Bill bill) {
        try {
            billService.getById(id);
            billService.updateBill(bill);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return bill;
    }

    @DeleteMapping("/{id}")
    public void deleteBill(@PathVariable int id) {
        try {
            billService.deleteBill(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
