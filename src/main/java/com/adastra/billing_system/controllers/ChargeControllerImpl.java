package com.adastra.billing_system.controllers;

import com.adastra.billing_system.exceptions.EntityNotFoundException;
import com.adastra.billing_system.models.Charge;
import com.adastra.billing_system.services.contracts.ChargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/charges")
public class ChargeControllerImpl {

    private final ChargeService chargeService;

    @Autowired
    public ChargeControllerImpl(ChargeService chargeService) {
        this.chargeService = chargeService;
    }

    @GetMapping("/all")
    public List<Charge> getAllCharges() {
        return chargeService.getAll();
    }

    @GetMapping("/{id}")
    public Charge getById(@PathVariable int id) {
        try {
            return chargeService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Charge createCharge(@RequestBody Charge charge) {
        chargeService.createCharge(charge);

        return charge;
    }

    @PutMapping("/{id}")
    public Charge updateCharge(@PathVariable int id, @RequestBody Charge charge) {
        try {
            chargeService.getById(id);
            chargeService.updateCharge(charge);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return charge;
    }

    @DeleteMapping("/{id}")
    public void deleteCharge(@PathVariable int id) {
        try {
            chargeService.deleteCharge(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
