package com.adastra.billing_system.exceptions;

public class NoDischargeDateException extends RuntimeException {

    public NoDischargeDateException(String message) {
        super(message);
    }
}
