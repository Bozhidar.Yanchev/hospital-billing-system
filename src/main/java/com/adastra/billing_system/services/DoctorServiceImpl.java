package com.adastra.billing_system.services;

import com.adastra.billing_system.models.Doctor;
import com.adastra.billing_system.repositories.contracts.DoctorRepository;
import com.adastra.billing_system.services.contracts.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DoctorServiceImpl implements DoctorService {

    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorServiceImpl(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    @Override
    public List<Doctor> getAll() {
        return doctorRepository.getAll();
    }

    @Override
    public Doctor getById(int id) {
        return doctorRepository.getById(id);
    }

    @Override
    public void createDoctor(Doctor doctor) {
        doctorRepository.create(doctor);
    }

    @Override
    public void updateDoctor(Doctor doctor) {
        doctorRepository.update(doctor);
    }

    @Override
    public void deleteDoctor(int id) {
        doctorRepository.delete(id);
    }
}
