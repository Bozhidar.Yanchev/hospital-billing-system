package com.adastra.billing_system.services;

import com.adastra.billing_system.models.Patient;
import com.adastra.billing_system.repositories.contracts.PatientRepository;
import com.adastra.billing_system.services.contracts.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientServiceImpl implements PatientService {

    private final PatientRepository patientRepository;

    @Autowired
    public PatientServiceImpl(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    @Override
    public List<Patient> getAll() {
        return patientRepository.getAll();
    }

    @Override
    public Patient getById(int id) {
        return patientRepository.getById(id);
    }

    @Override
    public void createPatient(Patient patient) {
        patientRepository.create(patient);
    }

    @Override
    public void updatePatient(Patient patient) {
        patientRepository.update(patient);
    }

    @Override
    public void deletePatient(int id) {
        patientRepository.delete(id);
    }
}
