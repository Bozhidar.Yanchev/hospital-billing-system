package com.adastra.billing_system.services.contracts;

import com.adastra.billing_system.models.Admission;
import com.adastra.billing_system.models.AdmissionDischarge;
import com.adastra.billing_system.models.Patient;

import java.util.List;

public interface AdmissionService {

    List<Admission> getAll();

    Admission getById(int id);

    List<Admission> getByPatient(Patient patient);

    AdmissionDischarge checkAdmissionDischargeDate(Admission admission);

    void createAdmission(Admission admission);

    void updateAdmission(Admission admission);

    void deleteAdmission(int id);

    void createAdmissionDischarge(AdmissionDischarge admissionDischarge);

}
