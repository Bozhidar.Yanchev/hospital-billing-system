package com.adastra.billing_system.services.contracts;

import com.adastra.billing_system.models.Charge;

import java.util.List;

public interface ChargeService {

    List<Charge> getAll();

    Charge getById(int id);

    void createCharge(Charge charge);

    void updateCharge(Charge charge);

    void deleteCharge(int id);
}
