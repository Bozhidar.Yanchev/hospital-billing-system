package com.adastra.billing_system.services.contracts;

import com.adastra.billing_system.models.Bill;
import com.adastra.billing_system.models.Patient;

import java.util.Date;
import java.util.List;

public interface BillService {

    List<Bill> getAll();

    Bill getById(int id);

    List<Bill> getByPatient(Patient patient, Date admissionDate, Date dischargeDate);

    void createBill(Bill bill);

    void updateBill(Bill bill);

    void deleteBill(int id);

}
