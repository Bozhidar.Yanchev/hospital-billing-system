package com.adastra.billing_system.services.contracts;

import com.adastra.billing_system.models.Patient;

import java.util.List;

public interface PatientService {

    List<Patient> getAll();

    Patient getById(int id);

    void createPatient(Patient patient);

    void updatePatient(Patient patient);

    void deletePatient(int id);
}
