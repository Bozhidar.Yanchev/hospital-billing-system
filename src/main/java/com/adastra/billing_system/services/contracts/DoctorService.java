package com.adastra.billing_system.services.contracts;

import com.adastra.billing_system.models.Doctor;
import java.util.List;

public interface DoctorService {

    List<Doctor> getAll();

    Doctor getById(int id);

    void createDoctor(Doctor doctor);

    void updateDoctor(Doctor doctor);

    void deleteDoctor(int id);
}
