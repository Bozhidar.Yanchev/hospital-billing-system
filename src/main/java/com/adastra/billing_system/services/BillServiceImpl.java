package com.adastra.billing_system.services;

import com.adastra.billing_system.models.Bill;
import com.adastra.billing_system.models.Patient;
import com.adastra.billing_system.repositories.contracts.BillRepository;
import com.adastra.billing_system.services.contracts.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class BillServiceImpl implements BillService {

    private final BillRepository billRepository;

    @Autowired
    public BillServiceImpl(BillRepository billRepository) {
        this.billRepository = billRepository;
    }

    @Override
    public List<Bill> getAll() {
        return billRepository.getAll();
    }

    @Override
    public Bill getById(int id) {
        return billRepository.getById(id);
    }

    @Override
    public List<Bill> getByPatient(Patient patient, Date admissionDate, Date dischargeDate) {
        return billRepository.getByPatient(patient, admissionDate, dischargeDate);
    }

    @Override
    public void createBill(Bill bill) {
        billRepository.create(bill);
    }

    @Override
    public void updateBill(Bill bill) {
        billRepository.update(bill);
    }

    @Override
    public void deleteBill(int id) {
        billRepository.delete(id);
    }
}
