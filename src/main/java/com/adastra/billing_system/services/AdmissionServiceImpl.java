package com.adastra.billing_system.services;

import com.adastra.billing_system.exceptions.EntityNotFoundException;
import com.adastra.billing_system.exceptions.NoDischargeDateException;
import com.adastra.billing_system.models.Admission;
import com.adastra.billing_system.models.AdmissionDischarge;
import com.adastra.billing_system.models.Bill;
import com.adastra.billing_system.models.Patient;
import com.adastra.billing_system.repositories.contracts.AdmissionDischargeRepository;
import com.adastra.billing_system.repositories.contracts.AdmissionRepository;
import com.adastra.billing_system.services.contracts.AdmissionService;
import com.adastra.billing_system.services.contracts.BillService;
import com.adastra.billing_system.services.contracts.ChargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class AdmissionServiceImpl implements AdmissionService {

    private static final String INVALID_DISCHARGE_DATE = "Discharge date cannot be before admission date!";
    private static final String PATIENT_NOT_DISCHARGED = "This patient is not discharged yet.";

    private final AdmissionRepository admissionRepository;

    private final AdmissionDischargeRepository admissionDischargeRepository;

    private final ChargeService chargeService;

    private final BillService billService;

    @Autowired
    public AdmissionServiceImpl(AdmissionRepository admissionRepository,
                                AdmissionDischargeRepository admissionDischargeRepository,
                                ChargeService chargeService, BillService billService) {
        this.admissionRepository = admissionRepository;
        this.admissionDischargeRepository = admissionDischargeRepository;
        this.chargeService = chargeService;
        this.billService = billService;
    }


    @Override
    public List<Admission> getAll() {
        return admissionRepository.getAll();
    }

    @Override
    public Admission getById(int id) {
        return admissionRepository.getById(id);
    }

    @Override
    public List<Admission> getByPatient(Patient patient) {
        return admissionRepository.getByPatient(patient);
    }

    @Override
    public AdmissionDischarge checkAdmissionDischargeDate(Admission admission) {
        try {
            return admissionDischargeRepository.getByField("admission", admission);
        } catch (EntityNotFoundException e) {
            throw new NoDischargeDateException(PATIENT_NOT_DISCHARGED);
        }
    }

    @Override
    public void createAdmission(Admission admission) {
        admissionRepository.create(admission);
    }

    @Override
    public void updateAdmission(Admission admission) {
        admissionRepository.update(admission);
    }

    @Override
    public void deleteAdmission(int id) {
        admissionRepository.delete(id);
    }

    @Override
    public void createAdmissionDischarge(AdmissionDischarge admissionDischarge) throws IllegalArgumentException {
        Date admissionDate = admissionDischarge.getAdmission().getAdmitted();
        if (admissionDischarge.getDischargeDate().before(admissionDate)) {
            throw new IllegalArgumentException(INVALID_DISCHARGE_DATE);
        }
        admissionDischargeRepository.create(admissionDischarge);

        long time = admissionDischarge.getDischargeDate().getTime() - admissionDate.getTime();
        long days = TimeUnit.DAYS.convert(time, TimeUnit.MILLISECONDS);

        for (int i = 0; i < days; i++) {

            createBill(admissionDate, admissionDischarge);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(admissionDate);
            calendar.add(Calendar.DATE, 1);
            admissionDate = calendar.getTime();
        }
    }

    private void createBill(Date admissionDate, AdmissionDischarge admissionDischarge) {
        Bill bill = new Bill();
        bill.setDate(admissionDate);
        bill.setCharge(chargeService.getById(2));
        bill.setPatient(admissionDischarge.getAdmission().getPatient());

        billService.createBill(bill);
    }
}
