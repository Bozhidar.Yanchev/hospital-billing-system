package com.adastra.billing_system.services.mappers;

import com.adastra.billing_system.models.AdmissionDischarge;
import com.adastra.billing_system.models.dtos.AdmissionDischargeDto;
import com.adastra.billing_system.services.contracts.AdmissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AdmissionDischargeMapper {

    private final AdmissionService admissionService;

    @Autowired
    public AdmissionDischargeMapper(AdmissionService admissionService) {
        this.admissionService = admissionService;
    }

    public AdmissionDischarge fromDto(AdmissionDischargeDto admissionDischargeDto) {
        AdmissionDischarge admissionDischarge = new AdmissionDischarge();


        admissionDischarge.setDischargeDate(admissionDischargeDto.getDischargeDate());
        return admissionDischarge;
    }

    public AdmissionDischargeDto toDto(AdmissionDischarge admissionDischarge) {
        AdmissionDischargeDto admissionDischargeDto = new AdmissionDischargeDto();
        admissionDischargeDto.setDischargeDate(admissionDischarge.getDischargeDate());

        return admissionDischargeDto;
    }
}
