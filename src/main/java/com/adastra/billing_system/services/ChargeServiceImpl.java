package com.adastra.billing_system.services;

import com.adastra.billing_system.models.Charge;
import com.adastra.billing_system.repositories.contracts.ChargeRepository;
import com.adastra.billing_system.services.contracts.ChargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChargeServiceImpl implements ChargeService {

    private final ChargeRepository chargeRepository;

    @Autowired
    public ChargeServiceImpl(ChargeRepository chargeRepository) {
        this.chargeRepository = chargeRepository;
    }

    @Override
    public List<Charge> getAll() {
        return chargeRepository.getAll();
    }

    @Override
    public Charge getById(int id) {
        return chargeRepository.getById(id);
    }

    @Override
    public void createCharge(Charge charge) {
        chargeRepository.create(charge);
    }

    @Override
    public void updateCharge(Charge charge) {
        chargeRepository.update(charge);
    }

    @Override
    public void deleteCharge(int id) {
        chargeRepository.delete(id);
    }
}
