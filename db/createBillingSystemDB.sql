CREATE DATABASE IF NOT EXISTS `billing_system`;
USE `billing_system`;

create table charges
(
    charge_id int auto_increment
        primary key,
    type      varchar(20) not null,
    price     double      not null,
    constraint charges_charge_id_uindex
        unique (charge_id)
);

create table patients
(
    patient_id    int auto_increment
        primary key,
    first_name    varchar(20) not null,
    last_name     varchar(20) not null,
    age           int         not null,
    date_of_birth date        not null,
    constraint patients_patient_id_uindex
        unique (patient_id)
);

create table admissions
(
    admission_id int auto_increment
        primary key,
    patient_id   int          not null,
    date_in      date         not null,
    diagnosis    varchar(30)  not null,
    treatment    varchar(128) not null,
    constraint admissions_admission_id_uindex
        unique (admission_id),
    constraint admissions_patients_fk
        foreign key (patient_id) references patients (patient_id)
);

create table admissions_discharge_date
(
    id             int auto_increment
        primary key,
    admission_id   int  not null,
    discharge_date date not null,
    constraint admissions_discharge_date_admissions_fk
        unique (admission_id),
    constraint admissions_discharge_date_id_uindex
        unique (id),
    constraint admissions_discharge_date_admissions_fk
        foreign key (admission_id) references admissions (admission_id)
);

create table patients_bills
(
    bill_id    int auto_increment
        primary key,
    patient_id int  not null,
    charge_id  int  not null,
    date       date not null,
    constraint patients_bills_bill_id_uindex
        unique (bill_id),
    constraint patients_bills_charges_fk
        foreign key (charge_id) references charges (charge_id),
    constraint patients_bills_patients_fk
        foreign key (patient_id) references patients (patient_id)
);

create table specialties
(
    specialty_id int auto_increment
        primary key,
    name         varchar(20) not null,
    constraint specialties_name_uindex
        unique (name),
    constraint specialties_specialty_id_uindex
        unique (specialty_id)
);

create table doctors
(
    doctor_id    int auto_increment
        primary key,
    first_name   varchar(20) not null,
    last_name    varchar(20) not null,
    specialty_id int         not null,
    constraint doctors_doctor_id_uindex
        unique (doctor_id),
    constraint doctors_specialties_specialty_id_fk
        foreign key (specialty_id) references specialties (specialty_id)
);

create table patients_doctors
(
    id         int auto_increment
        primary key,
    patient_id int  not null,
    doctor_id  int  not null,
    date       date not null,
    constraint patients_doctors_id_uindex
        unique (id),
    constraint patients_doctors_doctors_fk
        foreign key (doctor_id) references doctors (doctor_id),
    constraint patients_doctors_patients_fk
        foreign key (patient_id) references patients (patient_id)
);