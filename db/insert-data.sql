INSERT INTO billing_system.patients (patient_id, first_name, last_name, age, date_of_birth)
VALUES
    (1, 'Ivan', 'Ivanov', '25', '1997-05-05'),
    (2, 'Georgi', 'Georgiev', '27', '1995-03-10'),
    (3, 'Petar', 'Petrov', '2', '2020-07-15'),
    (4, 'Bozhidar', 'Yanchev', '27', '1995-07-20');

INSERT INTO billing_system.specialties (specialty_id, name)
VALUES
    (1, 'dentist'),
    (2,'surgeon'),
    (3, 'pediatrician');

INSERT INTO billing_system.doctors (doctor_id, first_name, last_name, specialty_id)
VALUES
    (1, 'Stefan', 'Petrov', 1),
    (2, 'Yanislav', 'Yanchev', 2),
    (3, 'Stoyan', 'Stoyanov', 3);


INSERT INTO billing_system.admissions (admission_id, patient_id, date_in, diagnosis, treatment)
VALUES
    (1, 1, '2022-03-03', 'tooth pain', 'medicine'),
    (2, 2, '2022-04-10', 'tooth pain', 'remove'),
    (3, 3, '2022-05-25', 'high temperature', 'medicine'),
    (4, 4, '2022-10-15', 'stomach pain', 'medicine');

INSERT INTO billing_system.admissions_discharge_date (id, admission_id, discharge_date)
VALUES
    (1, 1, '2022-03-03'),
    (2, 2, '2022-04-11'),
    (3, 3, '2022-05-27');

INSERT INTO billing_system.charges (charge_id, type, price)
VALUES
    (1, 'bed', 30),
    (2, 'medicine', 20),
    (3, 'doctors fee', 15);

INSERT INTO billing_system.patients_bills (bill_id, patient_id, charge_id, date)
VALUES
    (1, 1, 2, '2022-03-03'),
    (2, 1, 3, '2022-03-03'),
    (3, 2, 1, '2022-04-10'),
    (4, 2, 2, '2022-04-11'),
    (5, 2, 3, '2022-04-11'),
    (6, 3, 1, '2022-05-25'),
    (7, 3, 2, '2022-05-25'),
    (8, 3, 1, '2022-05-26'),
    (9, 3, 3, '2022-05-27');

INSERT INTO billing_system.patients_doctors (id, patient_id, doctor_id, date)
VALUES
    (1, 1, 1, '2022-03-03'),
    (2, 2, 1, '2022-04-11'),
    (3, 3, 3, '2022-04-11'),
    (4, 4, 2, '2022-10-15');